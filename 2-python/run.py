from random import random
from time import sleep

for i in range(10):
    print(f'progress {i*10}%')
    sleep(random())
print('done')
