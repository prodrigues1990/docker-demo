# Run

## Build

```
docker build -t 2-python:run . -f 1.run.Dockerfile
```

## Execute

```
docker run 2-python:run
```


# Cron

## Build

```
docker build -t 2-python:cron . -f 2.cron.Dockerfile
```

## Execute

```
docker run 2-python:cron
```
