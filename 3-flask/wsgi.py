from subprocess import Popen, PIPE
from flask import Flask, make_response

app = Flask(__name__)

@app.route('/')
def root():
    process = Popen(['ifconfig'], stdout=PIPE)
    r = make_response(process.communicate())
    r.headers['Content-type'] = r'text/plain;charset=utf-8'
    return r

if __name__ == '__main__':
    app.run()
