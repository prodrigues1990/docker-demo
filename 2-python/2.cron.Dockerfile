FROM python:3.8-slim

RUN apt-get update && apt-get -y install cron

ADD crontab /etc/cron.d/hello-cron
RUN chmod 0644 /etc/cron.d/hello-cron
RUN touch /var/log/cron.log

WORKDIR /opt
COPY run.py /opt/run.py

CMD cron && tail -f /var/log/cron.log
