# Build

```
docker build -t 3-flask .
```


# Execute

## Single host

```
docker run 3-flask
```

## Scaled

```
docker-compose up --scale app=3 --build
```

## Scaled distributed

```
rsync --exclude=venv * ds-mn-01:/opt/app
```

```
docker-compose up --scale app=3 --build
```
