FROM python:3.8-slim

WORKDIR /opt
COPY run.py /opt/run.py
CMD python run.py
