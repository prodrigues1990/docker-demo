# Build

```
docker build -t 1-apache .
```

# Execute

```
docker run --name 1-apache 1-apache
```
